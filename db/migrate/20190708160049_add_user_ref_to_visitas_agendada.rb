class AddUserRefToVisitasAgendada < ActiveRecord::Migration[5.2]
  def change
    add_reference :visitas_agendadas, :user, foreign_key: true
  end
end
