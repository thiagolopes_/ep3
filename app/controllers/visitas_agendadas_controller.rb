class VisitasAgendadasController < ApplicationController
  before_action :set_visitas_agendada, only: [:show, :edit, :update, :destroy]

  # GET /visitas_agendadas
  # GET /visitas_agendadas.json
  def index
    @visitas_agendadas = VisitasAgendada.all
  end

  # GET /visitas_agendadas/1
  # GET /visitas_agendadas/1.json
  def show
  end

  # GET /visitas_agendadas/new
  def new
    @visitas_agendada = VisitasAgendada.new
  end

  # GET /visitas_agendadas/1/edit
  def edit
  end

  # POST /visitas_agendadas
  # POST /visitas_agendadas.json
  def create
    @visitas_agendada = VisitasAgendada.new(visitas_agendada_params)

    respond_to do |format|
      if @visitas_agendada.save
        format.html { redirect_to visitas_agendadas_path }
        format.json { render :show, status: :created, location: @visitas_agendada }
      else
        format.html { render :new }
        format.json { render json: @visitas_agendada.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /visitas_agendadas/1
  # PATCH/PUT /visitas_agendadas/1.json
  def update
    respond_to do |format|
      if @visitas_agendada.update(visitas_agendada_params)
        format.html { redirect_to @visitas_agendada, notice: 'Visitas agendada was successfully updated.' }
        format.json { render :show, status: :ok, location: @visitas_agendada }
      else
        format.html { render :edit }
        format.json { render json: @visitas_agendada.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /visitas_agendadas/1
  # DELETE /visitas_agendadas/1.json
  def destroy
    @visitas_agendada.destroy
    respond_to do |format|
      format.html { redirect_to visitas_agendadas_url, notice: 'Visitas agendada was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visitas_agendada
      @visitas_agendada = VisitasAgendada.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def visitas_agendada_params
      params.require(:visitas_agendada).permit(:data, :hora, :user_id)
    end
end
