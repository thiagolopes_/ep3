Rails.application.routes.draw do
  resources :visitas_agendadas
  resources :cadastro_cachorros
  resources :contato
  resources :pagina_inicial
  devise_for :users
  get 'pagina_inicial/index'
  get 'contato/index'
  get 'login/index'
  get 'criar_conta/index'
  get 'cadastro_usuarios/index'

  root 'pagina_inicial#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
